
---------

# ABOUT THIS PROJECT #
I focused more on the Continuous Integration and the Architecture. When you first login in there is a delay, because I never included a spinning loader icon with an overlay etc. This is only a test.
Login via the socials buttons only, I only tested with Linkedin, not others, because giving out my data, & without any face-to-face meeting is illogical.

Databases, and stored procedures and serverside code I can present face-to-face.

Currently working on a new portfolio but a lot more then a portfolio. Currently building an application which includes various APIs, with ecommerce.
This is just some of the starting points and setting up the framework for bigger things to come. Busy on my current contract and my various start-up projects, 
so I only tested in Google Chrome, not other browser. For testing on login via gigya social, users will get access to a dashboard, with a list of friends from their networks.

### What is this repository for? ###

My new portfolio & more.
* Version
* [Mark Webley](https://bitbucket.org/front-end-developer/angular-portfolio-apis/src)

### How do I get set up? ###
* install node
* install and set up a Lamp stack, run a server on your local machine, for http://localhost
* cloud/server API files: copy the folder 'markwebley' to your lamp/http / xampp/http folder
* install database and store procedures: run sql commands in the file 'gigya_test.sql',
* run npm install
* run npm start


### I created an object of dummy contacts for the gigya login ###
LinkIn social contacts via gigya.

* angular 2 object, I created an object of dummy contacts for the gigya login, so that I could style the layouts, because my social test result came back with an empty array from gigya.
* Facebook and other social networks, I never tested, only coded and tested with LinkedIn, just to give you an idea.


## Front end Application Architecture & Development (full stack) ##
Built using the latest Front End technology stack today Feb 2017. The solution is using 
an Angular API simulator until the wiring on the front end is connected with the backend API.

* Angular 2
* Javascript
* ES6
* Typescript
* BootStrap 4
* CSS 3 & HTML5
* SASS installed but not yet used in the project
* Continuous integration
* Modular component based application architecture
* (not included in this demo) Pattern Library application architecture 
* How to run tests
* Deployment instructions
* full custom site database
* full custom site API (json / request, response, json)
* full custom site API methods for GET, POST, DELETE, PUT


## Custom API: Web / application developers notes & guidelines ##
The notes below are a review of what has been done and what is left to be done. 

* PHP
* PHP OOP and classes, with inheritance (no included in this version)
* Mysql/MariaDB Database
* Datbase custom Store Procedures
* **TODO:** check gigya user against site user and merge data.


**************
* **POST**
* LOGIN VIA THE SITE AS NORMAL
* **FINAL API:** http://localhost/markwebley/api/login 
*
* If login via site, it creates a session and sends that back until the users logs out.

API REQUEST DATA POSTED LIKE: 
can login via username and password
```javascript
	{
		"email":"email@example.com",
		"userpwd":"example3passwer#iuD"
	}
```

OR via email and password using the same fields for username and password:
```javascript
	{
		"email":"UserName",
		"userpwd":"example3passwer#iuD"
	}
```
  
API RESPONSE ON SUCCESS (RETURNS A UNIQUE SESSION & STATUS CODE 200):
```javascript
	{
	  "Request URL": "http://localhost/markwebley/api/login/",
	  "Request Method": "POST",
	  "Status Code": 200,
	  "Remote Address": "111.111.111.11",
	  "session": "vjfvu90v6ufh153ne8t6nmtm35"
	}
```

API RESPONSE ON FAILURE (RETURNS NO SESSION & STATUS CODE 401):
```javascript
	{
	  "Request URL": "http://localhost/markwebley/api/login/",
	  "Request Method": "POST",
	  "Status Code": 401,
	  "Remote Address": "111.111.111.11",
	  "session": null
	}
```

**************
* **POST**
* LOGIN VIA SOCIAL GIGYA WIDGET
* **FINAL API:** http://localhost/markwebley/api/login/gigya
*
* if login via social it checks if the gigya UID already exits and uses that data, 
* if no user gigya UID exits it creates it using the REQUEST PAYLOAD and then sends the session back to the client 

API REQUEST DATA POSTED LIKE: 
```javascript
	{
		"eventName":"_event_NAME_",
     	"timestamp":"0000-00-00 00:00:00",
     	"UID":"_gid_asfewOUOJLUOIJLsaf=",
     	"UIDSig":"sareIkasdfanl80adsaf0sss=",
     	"UIDSignature":"m0LX2abFPEq0MPt1diO70Ua4qQM=",
     	"contacts":"testeandblessed",
     	"displayname":"Social Network Name",
     	"friends":"testeandblessed",
     	"user_photoURL":"https://media.licdn.com/mpr/sfds/safasfsa-mkrsAKmjfzdsfDFDSA",
     	"user_firstname":"Mark",
     	"user_lastname":"Webley",
     	"user_email":"users@email.com",
     	"user_birthDay":"testeandblessed",
     	"user_birthMonth":"testeandblessed",
     	"user_birthYear":"testeandblessed",
     	"user_gender":"testeandblessed"
	}
```

API RESPONSE IF GIGYA UID DOES NOT EXIST:
NOTE: If the gigya login (UID not in my database) is a new user, payload returns:
```javascript
	{
	  "newGigyaUser": "true",
	  "newGigyaUserSession": "false",
	  "session": "tm0v04slphk0oh8uhnphttr8n6"
	}
```

API RESPONSE IF GIGYA UID EXIST FOR THIS USER:
```javascript
    {
	  "session": "tm0v04slphk0oh8uhnphttr8n6"
	}
```








  
**************
* **POST**
* **FINAL API:** http://localhost/markwebley/api/gigya/
* check if this is an existing gigya social user exits in the db:

API REQUEST:
```javascript
	{
		"uid": "_gid_serLObagPaseiiOJlKliOLAF="
	}
```

API RESPONSE IF GIGYA UID EXIST FOR THIS USER:
```javascript
	{
	  "gigya_UID": "_gid_PnO+/uY2oK7zDnYyfOaa0s5vKkRGH5iSlBwtVus2PCY=",
	  "gigya_UIDSig": "E7ebKQJVXC3YO7EuuAvV/6hJyLU=",
	  "gigya_UIDSignature": "RnwAGEmdezmfycNeVmh/4tx99EM=",
	  "gigya_timestamp": null,
	  "gigya_eventName": null,
	  "country": null,
	  "city": null,
	  "provider_displayname": null,
	  "provider_friends": null,
	  "provider_contacts": null,
	  "response_user_nickname": null,
	  "response_user_firstname": "Mark",
	  "response_user_lastname": "Webley",
	  "response_user_photoURL": null,
	  "response_user_birthDay": null,
	  "response_user_birthMonth": null,
	  "response_user_birthYear": null,
	  "response_user_gender": null,
	  "response_user_email": "mrkwebley@gmail.com",
	  "response_user": null
	}
```

API RESPONSE IF GIGYA UID DOES NOT EXIST FOR THIS USER:
```javascript
{
  "session": null
}
```

**************
* **GET**
* **FINAL API:**: http://localhost/markwebley/api/session/?session=session_keys_string_aservovi089vasrefi
* VERIFY USER SESSION ONLY: CHECK a user session MATCHES a current session in the db
payload request:
```javascript
    ajax url: http://localhost/markwebley/api/session/?session=session_keys_string_aservovi089vasrefi
```

API RESPONSE IF SESSION EXIST:
```javascript
    {
        "session_status": true,
        "status": 200
    }
```

API RESPONSE IF SESSION DOES NOT EXIST:
```javascript
    {
		"session_status": false,
		"status": 200
    }
```



 
**************
* **POST**
* **FINAL API:** http://localhost/markwebley/api/session/
* I CAN USE THIS API FOR TESTING ONLY FROM AJAX CLIENTSIDE BUT, SHOULD ONLY BE USED BY SERVERSIDE CODE ONLY.
* SESSION KEYS ARE BE DEFINED BY THE BACKEND ONLY.

* START A USER SESSION ONLY, 
* because is a social login, and not a site login, there is no userid from the site, so it is set to 0 (mysql sets the null value of an INT type to 0)
* Otherwise insert id.

* TODO: SEND A SESSION ID TO START THAT IS TAKEN FROM JAVASCRIPTS: sessionStorage, 

DO A GET REQUEST LIKE:
```javascript
    {
        "session": "tm0v04slphk0oh8uhnphttr8n6"
    }
```    

API RESPONSE:
```javascript
    {
		"session": "tm0v04slphk0oh8uhnphttr8n6",
		"status": 200
    }
	
	FOR TESTING NOW RESPONDS:
	{
	  "test_POST_session": "knc7tndfl9hkm9eetfakuvvh27",
	  "test_BACKEND_DEFINED_session": "tm0v04slphk0oh8uhnphttr8n6",
	  "session": "tm0v04slphk0oh8uhnphttr8n6",
	  "status": 200
	}
```


**************
TO BE CONFIRMED:

* **PUT**
* **FINAL API:** http://localhost/markwebley/api/session/
* STOP A CURRENT USER SESSION ONLY
* http://localhost/markwebley/api/db_connect.php?stop_session=sessionid_abcdefg

TO STOP THE CURRENT SESSION SEND A REQUEST LIKE:
```javascript
    {
        "session": null
    }
```  

TO STOP A SESSION, VIA PASSING IN THE current javascript sessionStorage session value, SEND A REQUEST LIKE:
```javascript
    {
        "session": "knc7tndfl9hkm9eetfakuvvh27"
    }
```      
 
 API RESPONSE:
    {
      "session_stopped": "tm0v04slphk0oh8uhnphttr8n6",
      "status": 200
    }
```  

 
# BUG FIXED #

When you look deep inside spawn-default-shell/src/get-shell.js Node Modules they are trying to compare our SHELL path with common shell regex.
const DETECT_SH_REGEX = /sh$/;
that variable assume that all shell name must end with "sh" while in our case it turns out to be
C:\Program Files\Git\bin\bash.exe
which means ends with "sh.exe" then it trows an error.

* so the solution is change
* const DETECT_SH_REGEX = /sh$/;
* to
* const DETECT_SH_REGEX = /sh/;

# WHAT I DID NOT FINISH - FOR THE TEST #
- the gigya login for linked in, used dummy data for contacts to simulate the view, because nothing came in from the backend at gigya, a feature has to be enabled in the gigya portal,


# WHAT I DID NOT FINISH - EXTRA STUFF #
- the website login (not the gigya one) and wireing from the front end Angular 2, Observable & Promises to the custom API backend...is underconstruction for my new personal portfolio,
- comments in code for me to do on my personal portfolio site - I am currently working on.
- Angular 2 stuff, Angular changed drastically from previous version but for the better

### Author ###

* Repo owner & admin: Mark Webley
