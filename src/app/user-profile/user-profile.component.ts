import { Component, OnInit} from '@angular/core';
import { Location }                 from '@angular/common';
import { ActivatedRoute, Params }   from '@angular/router';
import {GigyaSessionService} from "./../services/gigya-session.service";
import {WindowRef} from "./../common/window.component";

@Component({
  moduleId: module.id,
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
  public gigya:any;
  public user:any;
  public contacts:any = [];

  constructor(public winRef : WindowRef, private gigyaSessionService: GigyaSessionService,
              private route: ActivatedRoute, private location: Location) {
    this.gigya = winRef.nativeWindow.gigya;
  }
  ngOnInit(): void {

    this.user = {
      UID: null,
      isSiteUID: null,
      UIDSignature: null,
      profileNickname: null,
      profileFirstname: null,
      profileLastname: null,
      profileTime: null,
      profileEmail: null,
      profileCountry: null,
      profileCity: null,
      profilePhoto: null,
      profileURL: null,
      profileIsLoggedIn: null,
      profileIsConnected: null,
      profileLoginProvider: null
    }
    this.checkSession();
    this.initLoad();
  }

  init(){
    var options = {
      version: '2',
      width: '100%',
      context: this,
      containerID: 'dashboard-social',
      headerText: 'Welcome to social solutions',
      showTermsLink: false,
      actionAttributes: {
        page: 'user dashboard'
      },
      // Gamification to implement
      //actionAttributes: 'home-page-login',
      // TODO: authCodeONly
      enabledProviders: 'facebook,twitter,googleplus,linkedin,paypal',
      cid: '',
      sessionExpiration: '-2',  // use for testing only
      onError: this.onErrorHandler,
      onConnectionAdded: this.onConnectionAddedHandler,
      onButtonClicked: this.onButtonClickedHandler
    }
    this.gigya.socialize.getFriendsInfo({context: this,callback: this.getFriendsData}); //

    // register for connect status changes
    this.gigya.socialize.addEventHandlers({ onConnectionAdded: this.renderListComponent, onConnectionRemoved: this.renderListComponent });
    this.gigya.socialize.showAddConnectionsUI(options);
  }

  checkSession(){
    if (!this.gigyaSessionService.checkSession()) {
      this.gigyaSessionService.destroy();
      document.location.href = '/login';
      //this.route.navigate(['/login'])
    }
  }

  getSiteSession(){
    return this.gigyaSessionService.get();
  }

  initLoad() {
    let gigyaSession= this.gigyaSessionService.get();
    let isGigyaUserUID:string = null;
    let isGigyaUserUIDSig:string = null;

      /**
       * TODO:
       * @description     match the session with the session key active for this user in the database, this.gigyaSessionService.getSessionKey()
       * GET:             http://localhost/markwebley/api/session/?session=session_keys_string_taken_from_session_TO_  (      this.gigyaSessionService.getSessionKey()   )
       * @type {string|any}
       */
    let isSiteSessionUser = sessionStorage.getItem('appijuiceSession');
    if (gigyaSession || isSiteSessionUser){
      this.user.UID =                   gigyaSession.UID;
      this.user.isSiteUID =             gigyaSession.isSiteUID;
      this.user.UIDSignature =          gigyaSession.UIDSignature;
      this.user.profileNickname =       gigyaSession.user.nickname;
      this.user.profileFirstname =      gigyaSession.user.firstName;
      this.user.profileLastname =       gigyaSession.user.lastName;
      this.user.profileTime =           gigyaSession.user.time;
      this.user.profileEmail =          gigyaSession.user.email;
      this.user.profileCountry =        gigyaSession.user.country;
      this.user.profileCity =           gigyaSession.user.city;
      this.user.profilePhoto =          gigyaSession.user.photoURL;
      this.user.profileURL =            gigyaSession.user.profileURL;
      this.user.profileIsLoggedIn =     gigyaSession.user.isLoggedIn;
      this.user.profileIsConnected =    gigyaSession.user.isConnected;
      this.user.profileLoginProvider =  gigyaSession.user.loginProvider;
      this.init();
    } else {
      // this.route.navigate(['/login']);
      document.location.href = '/login';
    }
  }

  onErrorHandler(e: any) {
    console.log('onErrorHander', e);
  }

  /**
   * TODO:
   * MEEDS TP BE ENABLED IN THE GIGYA CONTROL PANEL, FOR YOU TO SEE A COLLECTION OF FRIENDS.
   *
   * http://developers.gigya.com/display/GD/socialize.getFriendsInfo+JS
   * Note that retrieving friends' data should be requested explicitly by
   * checking the appropriate checkbox in the console's permissions section.
   */
   getFriendsData(res: any){
          console.log(res);

          // TODO: NASTY HACK TO TEST, AND STYLE DATA, BECAUSE THE ARRAY CAME UP EMPTY FROM GIGYA
          // ONLY TESTED WITH LINKEDIN, NOT WITH OTHER SOCIAL NETWORKS...
          // TODO: DUMMY OBJECT BECAUSE MY SOCIAL ARRAY OBJECT ON GIGYA WAS EMPTY
          // social test model
          res.friends[0] = {};
          res.friends[0].isSiteUID = '';
          res.friends[0].isSiteUser = '';
          res.friends[0].thumbnailURL = 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg';
          res.friends[0].nickname = 'Jackie Chan';

          res.friends[1] = {};
          res.friends[1].isSiteUID = '';
          res.friends[1].isSiteUser = '';
          res.friends[1].thumbnailURL = 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg';
          res.friends[1].nickname = 'Suzzie Chan';

          res.friends[2] = {};
          res.friends[2].isSiteUID = '';
          res.friends[2].isSiteUser = '';
          res.friends[2].thumbnailURL = 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg';
          res.friends[2].nickname = 'Jason Wong';


          res.context.contacts.push({nickname: 'Jackie Chan', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'Suzzie Chan', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'Jason Chan', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'Mark', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'Mathew', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'Luke', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'John', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'Acts', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
          res.context.contacts.push({nickname: 'Romans', thumbnailURL : 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg'});
  }

  onConnectionAddedHandler(e: any) {
          console.log('onConnectionAddedHander', e.provider, ' context object test', e.context, ' user object', e.user);
          this.gigya.socialize.getFriendsInfo({ callback: e.context.getFriendsData});
  }

  onButtonClickedHandler(e: any) {
      // click track
  }

  // render the UI
  renderListComponent(result: any){
  }

  logout() {
    // call serverside method
    this.gigyaSessionService.destroy(this.gigya, this.logoutHandler);
    document.location.href = '/login';
  }

  logoutHandler(response: any) {
          debugger;
          if ( response.errorCode == 0 ) {
            var msg = 'User has logged out';
            if (response.connectedProviders) {
              msg += ' from, ' + response.connectedProviders;
            }
            document.location.href = '/login';
          }
          else {
            console.error('User has logout Error :' + response.errorMessage);
          }
    }
}
