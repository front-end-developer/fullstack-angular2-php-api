"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var gigya_session_service_1 = require("./../services/gigya-session.service");
var window_component_1 = require("./../common/window.component");
var UserProfileComponent = (function () {
    function UserProfileComponent(winRef, gigyaSessionService, route, location) {
        this.winRef = winRef;
        this.gigyaSessionService = gigyaSessionService;
        this.route = route;
        this.location = location;
        this.contacts = [];
        this.gigya = winRef.nativeWindow.gigya;
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        this.user = {
            UID: null,
            isSiteUID: null,
            UIDSignature: null,
            profileNickname: null,
            profileFirstname: null,
            profileLastname: null,
            profileTime: null,
            profileEmail: null,
            profileCountry: null,
            profileCity: null,
            profilePhoto: null,
            profileURL: null,
            profileIsLoggedIn: null,
            profileIsConnected: null,
            profileLoginProvider: null
        };
        this.checkSession();
        this.initLoad();
    };
    UserProfileComponent.prototype.init = function () {
        var options = {
            version: '2',
            width: '100%',
            context: this,
            containerID: 'dashboard-social',
            headerText: 'Welcome to social solutions',
            showTermsLink: false,
            actionAttributes: {
                page: 'user dashboard'
            },
            // Gamification to implement
            //actionAttributes: 'home-page-login',
            // TODO: authCodeONly
            enabledProviders: 'facebook,twitter,googleplus,linkedin,paypal',
            cid: '',
            sessionExpiration: '-2',
            onError: this.onErrorHandler,
            onConnectionAdded: this.onConnectionAddedHandler,
            onButtonClicked: this.onButtonClickedHandler
        };
        this.gigya.socialize.getFriendsInfo({ context: this, callback: this.getFriendsData }); //
        // register for connect status changes
        this.gigya.socialize.addEventHandlers({ onConnectionAdded: this.renderListComponent, onConnectionRemoved: this.renderListComponent });
        this.gigya.socialize.showAddConnectionsUI(options);
    };
    UserProfileComponent.prototype.checkSession = function () {
        if (!this.gigyaSessionService.checkSession()) {
            this.gigyaSessionService.destroy();
            document.location.href = '/login';
        }
    };
    UserProfileComponent.prototype.getSiteSession = function () {
        return this.gigyaSessionService.get();
    };
    UserProfileComponent.prototype.initLoad = function () {
        var gigyaSession = this.gigyaSessionService.get();
        var isGigyaUserUID = null;
        var isGigyaUserUIDSig = null;
        /**
         * TODO:
         * @description     match the session with the session key active for this user in the database, this.gigyaSessionService.getSessionKey()
         * GET:             http://localhost/markwebley/api/session/?session=session_keys_string_taken_from_session_TO_  (      this.gigyaSessionService.getSessionKey()   )
         * @type {string|any}
         */
        var isSiteSessionUser = sessionStorage.getItem('appijuiceSession');
        if (gigyaSession || isSiteSessionUser) {
            this.user.UID = gigyaSession.UID;
            this.user.isSiteUID = gigyaSession.isSiteUID;
            this.user.UIDSignature = gigyaSession.UIDSignature;
            this.user.profileNickname = gigyaSession.user.nickname;
            this.user.profileFirstname = gigyaSession.user.firstName;
            this.user.profileLastname = gigyaSession.user.lastName;
            this.user.profileTime = gigyaSession.user.time;
            this.user.profileEmail = gigyaSession.user.email;
            this.user.profileCountry = gigyaSession.user.country;
            this.user.profileCity = gigyaSession.user.city;
            this.user.profilePhoto = gigyaSession.user.photoURL;
            this.user.profileURL = gigyaSession.user.profileURL;
            this.user.profileIsLoggedIn = gigyaSession.user.isLoggedIn;
            this.user.profileIsConnected = gigyaSession.user.isConnected;
            this.user.profileLoginProvider = gigyaSession.user.loginProvider;
            this.init();
        }
        else {
            // this.route.navigate(['/login']);
            document.location.href = '/login';
        }
    };
    UserProfileComponent.prototype.onErrorHandler = function (e) {
        console.log('onErrorHander', e);
    };
    /**
     * TODO:
     * MEEDS TP BE ENABLED IN THE GIGYA CONTROL PANEL, FOR YOU TO SEE A COLLECTION OF FRIENDS.
     *
     * http://developers.gigya.com/display/GD/socialize.getFriendsInfo+JS
     * Note that retrieving friends' data should be requested explicitly by
     * checking the appropriate checkbox in the console's permissions section.
     */
    UserProfileComponent.prototype.getFriendsData = function (res) {
        console.log(res);
        // TODO: NASTY HACK TO TEST, AND STYLE DATA, BECAUSE THE ARRAY CAME UP EMPTY FROM GIGYA
        // ONLY TESTED WITH LINKEDIN, NOT WITH OTHER SOCIAL NETWORKS...
        // TODO: DUMMY OBJECT BECAUSE MY SOCIAL ARRAY OBJECT ON GIGYA WAS EMPTY
        // social test model
        res.friends[0] = {};
        res.friends[0].isSiteUID = '';
        res.friends[0].isSiteUser = '';
        res.friends[0].thumbnailURL = 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg';
        res.friends[0].nickname = 'Jackie Chan';
        res.friends[1] = {};
        res.friends[1].isSiteUID = '';
        res.friends[1].isSiteUser = '';
        res.friends[1].thumbnailURL = 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg';
        res.friends[1].nickname = 'Suzzie Chan';
        res.friends[2] = {};
        res.friends[2].isSiteUID = '';
        res.friends[2].isSiteUser = '';
        res.friends[2].thumbnailURL = 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg';
        res.friends[2].nickname = 'Jason Wong';
        res.context.contacts.push({ nickname: 'Jackie Chan', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'Suzzie Chan', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'Jason Chan', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'Mark', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'Mathew', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'Luke', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'John', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'Acts', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
        res.context.contacts.push({ nickname: 'Romans', thumbnailURL: 'http://hotprogrammers.co.uk/images/hotprogrammers.jpg' });
    };
    UserProfileComponent.prototype.onConnectionAddedHandler = function (e) {
        console.log('onConnectionAddedHander', e.provider, ' context object test', e.context, ' user object', e.user);
        this.gigya.socialize.getFriendsInfo({ callback: e.context.getFriendsData });
    };
    UserProfileComponent.prototype.onButtonClickedHandler = function (e) {
        // click track
    };
    // render the UI
    UserProfileComponent.prototype.renderListComponent = function (result) {
    };
    UserProfileComponent.prototype.logout = function () {
        // call serverside method
        this.gigyaSessionService.destroy(this.gigya, this.logoutHandler);
        document.location.href = '/login';
    };
    UserProfileComponent.prototype.logoutHandler = function (response) {
        debugger;
        if (response.errorCode == 0) {
            var msg = 'User has logged out';
            if (response.connectedProviders) {
                msg += ' from, ' + response.connectedProviders;
            }
            document.location.href = '/login';
        }
        else {
            console.error('User has logout Error :' + response.errorMessage);
        }
    };
    UserProfileComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user-profile',
            templateUrl: './user-profile.component.html',
            styleUrls: ['./user-profile.component.css']
        }), 
        __metadata('design:paramtypes', [window_component_1.WindowRef, gigya_session_service_1.GigyaSessionService, router_1.ActivatedRoute, common_1.Location])
    ], UserProfileComponent);
    return UserProfileComponent;
}());
exports.UserProfileComponent = UserProfileComponent;
//# sourceMappingURL=user-profile.component.js.map