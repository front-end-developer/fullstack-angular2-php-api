import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from "./router/app-routing.module";
import { AppComponent }  from './app.component';
import {LoginFormComponent} from "./login/login-form.component";
import {GigyaFormComponent} from "./gigya-login/gigya-form.component";
import {WindowRef} from "./common/window.component";
import {UserService} from "./services/user/user.service";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {GigyaSessionService} from "./services/gigya-session.service";
import {ApplicationService} from "./services/application.service.ts/application.service";

@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    AppRoutingModule
  ],

  declarations: [
    AppComponent,
    LoginFormComponent,
    GigyaFormComponent,
    UserProfileComponent
  ],

  providers: [
    GigyaSessionService,
    UserService,
    ApplicationService,
    WindowRef
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
