"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var window_component_1 = require("../common/window.component");
var user_service_1 = require("../services/user/user.service");
var gigya_session_service_1 = require("../services/gigya-session.service");
var GigyaFormComponent = (function () {
    function GigyaFormComponent(winRef, userService, gigyaSessionService, router, route, location) {
        this.winRef = winRef;
        this.userService = userService;
        this.gigyaSessionService = gigyaSessionService;
        this.router = router;
        this.route = route;
        this.location = location;
        this.submitted = false;
        this.gigya = winRef.nativeWindow.gigya;
        this.checkSession();
    }
    GigyaFormComponent.prototype.ngOnInit = function () {
        this.checkSession();
    };
    GigyaFormComponent.prototype.initGigya = function () {
        var options = {
            version: '2',
            width: '100%',
            containerID: 'gigya-login-component',
            headerText: 'Rapid login via your social network',
            showTermsLink: false,
            context: this,
            actionAttributes: {
                page: 'gigya login component'
            },
            // Gamification to implement
            //actionAttributes: 'home-page-login',
            // TODO: authCodeONly
            buttonStyle: 'fullLogoColored',
            buttonSize: 55,
            enabledProviders: 'facebook,twitter,googleplus,linkedin,paypal',
            pagingButtonStyle: 'newArrows',
            cid: '',
            sessionExpiration: '-2',
            callback: this.loginNonSocialHandler,
            onError: this.onErrorHander,
            onLoad: this.onLoadHander,
            onLogin: this.onLoginHander,
            onButtonClicked: this.onButtonClickedHander
        };
        this.gigya.socialize.showLoginUI(options);
    };
    GigyaFormComponent.prototype.onLogin = function (evt) {
        evt.preventDefault();
        this.loginNonSocialHandler({});
    };
    GigyaFormComponent.prototype.onErrorHander = function (e) {
        console.log('error: ' + e.errorMessage);
    };
    GigyaFormComponent.prototype.checkSession = function () {
        if (this.gigyaSessionService.checkSession()) {
            document.location.href = '/my-profile';
        }
        else {
            // clear session to clean up data
            this.gigyaSessionService.destroy();
            this.initGigya();
        }
    };
    GigyaFormComponent.prototype.onLoadHander = function (e) {
        this.checkSession();
    };
    GigyaFormComponent.prototype.emailNotValid = function (e) {
        var regEmail = /^[A-Za-z0-9\.]{1,64}@{1}(?:[A-Za-z0-9]{1,64}\.){1,64}[A-za-z]{2,7}$/;
        return regEmail.test(e);
    };
    GigyaFormComponent.prototype.addShowEmailModal = function () {
        console.warn("user email not valid show modal window");
    };
    GigyaFormComponent.prototype.setUserGigyaSession = function (e) {
        var gigyaAPI = {
            apiDomaine: e.response.requestParams.apiDomain
        };
        var gigyaProvider = {
            id: e.provider.ID,
            displayName: e.provider.displayName,
            name: e.provider.name,
        };
        var gigyaUser = {
            loginProviderUID: e.user.loginProviderUID,
            loginProvider: e.user.loginProvider,
            photoURL: e.user.photoURL,
            profileURL: e.user.profileURL,
            nickname: e.user.nickname,
            firstName: e.user.firstName,
            lastName: e.user.lastName,
            email: e.user.email,
            country: e.user.country,
            city: e.user.city,
            time: e.user.time,
            timestamp: e.user.timestamp,
            isSiteUser: e.user.isSiteUser,
            isLoggedIn: e.user.isLoggedIn,
            isConnected: e.user.isConnected
        };
        var gigyaSession = {
            UID: e.UID,
            UIDSig: e.UIDSig,
            UIDSignature: e.UIDSignature,
            isSiteUID: e.isSiteUID,
            user: gigyaUser,
            provider: gigyaProvider,
            gigyaAPI: gigyaAPI
        };
        /**
         * set gigya session in my database
         */
        return e.context.gigyaSessionService.postUserGigyaData(e).then(function (res) {
            if (res.session.length > 10) {
                e.context.gigyaSessionService.set(JSON.stringify(gigyaSession));
            }
            else {
                console.log('do some notification error in the UI');
            }
            return res;
        });
    };
    GigyaFormComponent.prototype.handleError = function (error) {
        console.error('PROMISES VERSION:- An error occurred', error);
        return Promise.reject(error.message || error);
    };
    GigyaFormComponent.prototype.onLoginHander = function (e) {
        // check that it is not a social login, do this only for the login form
        if (!e.context.emailNotValid(e.response.user.email)) {
            e.context.addShowEmailModal();
            return;
        }
        // 1. Registers user
        // 2. Stores new user in DB
        // 3. link site account to social network identity
        // 3.1 first construct the linkAccounts parameters
        e.context.setUserGigyaSession(e).then(function (res) {
            /**
             * Store user session taken created from my DB
             **/
            if (res.session.length > 10) {
                debugger;
                e.context.gigyaSessionService.createSession(res.session);
                /**
                 * db response tells me if this is a new user via the gigya's UID
                 */
                var newUser = res.newGigyaUser == "true" ? true : false;
                /*
                * user siteUID from my DB taken from the new user record,
                * TODO: otherwise use a fake one, untill I finish that thing in the DB
                */
                var siteUID = res.userSiteUID ? res.userSiteUID : 'uTtCGqDTEtcZMGL08w';
                /**TODO: temporary forced boolean to true:
                 * ALWAYS SET TO TRUE FOR TEST ETC, NO REASON NOT TO ADD USERS
                 * lets assume the user is new
                 */
                newUser = true;
                if (newUser) {
                    var dateStr = Math.round(new Date().getTime() / 1000.0); // Current time in Unix format
                    //(i.e. the number of seconds since Jan. 1st 1970)
                    var yourSig = e.context.createSignature(siteUID, dateStr);
                    var params = {
                        siteUID: siteUID,
                        timestamp: dateStr,
                        cid: '',
                        signature: yourSig
                    };
                    e.context.gigya.socialize.notifyRegistration(params);
                }
                // IF gigya social login is successful
                if (e.UID) {
                    e.context.router.navigate(['/my-profile']);
                }
            }
            else {
                console.log('Error user session could not be created for my site');
            }
        })
            .catch(this.handleError);
    };
    GigyaFormComponent.prototype.isGigyaUserAuthorised = function (gigyaUserData) {
        var that = this;
        this.userService.isGigyaUserAuthorised(gigyaUserData)
            .then(function (result) {
            console.log('result from the serice:- ', result);
            if (result.status == 200) {
                result.callback(result.context);
                console.info('YOU ARE authenticated, go to login');
            }
            else {
                console.warn('show error to the user, they are not authenticated');
            }
        }, function (e) {
            console.log('error after result IN isUserAuthorised FUNC');
        });
    };
    GigyaFormComponent.prototype.gigyaUserSession = function (e) {
        // place into a separate service object
        var gigyaData = {
            eventName: "__event_NAME",
            timestamp: "0000-00-00 00:00:00",
            UID: e.UID,
            UIDSig: e.UIDSig,
            UIDSignature: e.UIDSignature,
            contacts: "testeandblessed",
            displayname: e.provider.displayName,
            friends: "testeandblessed",
            user_photoURL: e.user.photoURL,
            user_firstname: e.user.firstName,
            user_lastname: e.user.lastName,
            user_email: e.user.email,
            user_birthDay: "",
            user_birthMonth: "",
            user_birthYear: "",
            user_gender: ""
        };
        return gigyaData;
    };
    // Note: the actual signature calculation implementation should be on server side
    GigyaFormComponent.prototype.createSignature = function (UID, timestamp) {
        var encodedUID = encodeURIComponent(UID + timestamp); // encode the UID parameter before sending it to the server.
        // On server side use decodeURIComponent() function to decode an encoded UID
        return encodedUID;
    };
    /*
    * @description  use for click tracking
     */
    GigyaFormComponent.prototype.onButtonClickedHander = function (e) {
        console.log('onButtonClicked: ' + e);
    };
    /**
     * do some other stuff
       */
    GigyaFormComponent.prototype.loginNonSocialHandler = function (response) {
        // do ajax call to my cloud server
        //let context = response.context || this;
        //context.route.navigate(['dashboard']);
        // do a serverside redirect
    };
    /**
     * to finish off
     */
    GigyaFormComponent.prototype.onSubmit = function (evt) {
        evt.preventDefault();
        this.loginNonSocialHandler({});
        this.submitted = true;
    };
    GigyaFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'gigya-login',
            templateUrl: './gigya-form.component.html',
            styleUrls: ['./gigya.css']
        }), 
        __metadata('design:paramtypes', [window_component_1.WindowRef, user_service_1.UserService, gigya_session_service_1.GigyaSessionService, router_1.Router, router_1.ActivatedRoute, common_1.Location])
    ], GigyaFormComponent);
    return GigyaFormComponent;
}());
exports.GigyaFormComponent = GigyaFormComponent;
//# sourceMappingURL=gigya-form.component.js.map