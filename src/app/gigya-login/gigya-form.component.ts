import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute, RouterModule, Router, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import {WindowRef}                  from "../common/window.component";
import {UserService}                from "../services/user/user.service";
import {GigyaSessionService}        from "../services/gigya-session.service";

@Component({
  moduleId: module.id,
  selector: 'gigya-login',
  templateUrl: './gigya-form.component.html',
  styleUrls: ['./gigya.css']
})

export class GigyaFormComponent implements OnInit {

  public submitted = false;
  public gigya:any;
  public gigyaData:any;

 constructor(public winRef : WindowRef,
             private userService: UserService,
             private gigyaSessionService: GigyaSessionService,
             private router: Router,
             private route: ActivatedRoute,
             private location: Location) {
    this.gigya = winRef.nativeWindow.gigya;
    this.checkSession();
 }

  ngOnInit(): void {
    this.checkSession();
  }

  initGigya(){
    var options = {
      version: '2',
      width: '100%',
      containerID: 'gigya-login-component',
      headerText: 'Rapid login via your social network',
      showTermsLink: false,
      context: this,
      actionAttributes: {
        page: 'gigya login component'
      },

      // Gamification to implement
      //actionAttributes: 'home-page-login',
      // TODO: authCodeONly

      buttonStyle: 'fullLogoColored',
      buttonSize: 55,
      enabledProviders: 'facebook,twitter,googleplus,linkedin,paypal',
      pagingButtonStyle: 'newArrows',
      cid: '',

      sessionExpiration: '-2',  // use for testing only
      callback: this.loginNonSocialHandler,
      onError: this.onErrorHander,
      onLoad: this.onLoadHander,
      onLogin: this.onLoginHander,
      onButtonClicked: this.onButtonClickedHander
    }
    this.gigya.socialize.showLoginUI(options);
  }

  onLogin(evt: any){
    evt.preventDefault();
    this.loginNonSocialHandler({});
  }

  onErrorHander(e: any) {
    console.log('error: ' + e.errorMessage);
  }

  checkSession(){
      if (this.gigyaSessionService.checkSession()) {
        document.location.href = '/my-profile';
      } else {
        // clear session to clean up data
        this.gigyaSessionService.destroy();
        this.initGigya();
      }
  }

  onLoadHander(e:any) {
    this.checkSession();
  }

  emailNotValid(e: string) {
    var regEmail = /^[A-Za-z0-9\.]{1,64}@{1}(?:[A-Za-z0-9]{1,64}\.){1,64}[A-za-z]{2,7}$/;
    return regEmail.test(e);
  }

  addShowEmailModal() {
    console.warn("user email not valid show modal window");
  }

  setUserGigyaSession(e:any) {

    let gigyaAPI = {
      apiDomaine:   e.response.requestParams.apiDomain
    };
    let gigyaProvider = {
      id:           e.provider.ID,
      displayName:  e.provider.displayName,
      name:         e.provider.name,
    };
    let gigyaUser = {
      loginProviderUID: e.user.loginProviderUID,
      loginProvider:    e.user.loginProvider,
      photoURL:         e.user.photoURL,
      profileURL:       e.user.profileURL,
      nickname:         e.user.nickname,
      firstName:        e.user.firstName,
      lastName:         e.user.lastName,
      email:            e.user.email,
      country:          e.user.country,
      city:             e.user.city,
      time:             e.user.time,
      timestamp:        e.user.timestamp,
      isSiteUser:       e.user.isSiteUser,
      isLoggedIn:       e.user.isLoggedIn,
      isConnected:      e.user.isConnected
    };
    let gigyaSession = {
      UID:          e.UID,    // USE THIS TO LOG THE USER IN
      UIDSig:       e.UIDSig,
      UIDSignature: e.UIDSignature,
      isSiteUID:    e.isSiteUID,
      user:         gigyaUser,
      provider:     gigyaProvider,
      gigyaAPI:     gigyaAPI
    }

      /**
       * set gigya session in my database
       */
      return e.context.gigyaSessionService.postUserGigyaData(e).then((res:any) => {
          if (res.session.length > 10) {
            e.context.gigyaSessionService.set(JSON.stringify(gigyaSession));
          } else {
            console.log('do some notification error in the UI');
          }
        return res;
      });
  }

  handleError(error: any): Promise<any> {
    console.error('PROMISES VERSION:- An error occurred', error);
    return Promise.reject(error.message || error);
  }

  onLoginHander(e: any) {

      // check that it is not a social login, do this only for the login form
      if (!e.context.emailNotValid(e.response.user.email)) {
        e.context.addShowEmailModal();
        return;
      }

      // 1. Registers user
      // 2. Stores new user in DB
      // 3. link site account to social network identity
      // 3.1 first construct the linkAccounts parameters
      e.context.setUserGigyaSession(e).then((res:any) => {

        /**
         * Store user session taken created from my DB
         **/
        if ( res.session.length > 10 ) {
            debugger;

            e.context.gigyaSessionService.createSession(res.session);

            /**
             * db response tells me if this is a new user via the gigya's UID
             */
            let newUser:boolean = res.newGigyaUser == "true" ? true : false;

            /*
            * user siteUID from my DB taken from the new user record,
            * TODO: otherwise use a fake one, untill I finish that thing in the DB
            */
            let siteUID:number = res.userSiteUID ? res.userSiteUID : 'uTtCGqDTEtcZMGL08w';

            /**TODO: temporary forced boolean to true:
             * ALWAYS SET TO TRUE FOR TEST ETC, NO REASON NOT TO ADD USERS
             * lets assume the user is new
             */
            newUser = true;

            if (newUser) {
              let dateStr = Math.round(new Date().getTime() / 1000.0); // Current time in Unix format
              //(i.e. the number of seconds since Jan. 1st 1970)


              let yourSig = e.context.createSignature(siteUID, dateStr);
              let params = {
                siteUID: siteUID,
                timestamp: dateStr,
                cid: '',
                signature: yourSig
              };
              e.context.gigya.socialize.notifyRegistration(params);
            }

            // IF gigya social login is successful
            if (e.UID) {
              e.context.router.navigate(['/my-profile']);
              //document.location.href = '/my-profile';
            }
        } else {
          console.log('Error user session could not be created for my site');
        }
      })
      .catch(this.handleError);
  }

  isGigyaUserAuthorised(gigyaUserData:any){
    let that = this;
    this.userService.isGigyaUserAuthorised(gigyaUserData)
      .then(function(result) {
          console.log('result from the serice:- ', result);
          if (result.status == 200) {
            result.callback(result.context);
            console.info('YOU ARE authenticated, go to login');
          } else {
            console.warn('show error to the user, they are not authenticated');
          }
        },
        function(e){
          console.log('error after result IN isUserAuthorised FUNC');
        })
  }

  gigyaUserSession(e:any) {
    // place into a separate service object
    let gigyaData = {
      eventName:        "__event_NAME",
      timestamp:        "0000-00-00 00:00:00",
      UID:              e.UID, // _gid_PnO+/uY2oK7zDnYyfOaa0s5vKkRGH5iSlBwtVus2PCY=
      UIDSig:           e.UIDSig,
      UIDSignature:     e.UIDSignature,
      contacts:         "testeandblessed",
      displayname:        e.provider.displayName,
      friends:          "testeandblessed",
      user_photoURL:     e.user.photoURL,
      user_firstname:   e.user.firstName,
      user_lastname:    e.user.lastName,
      user_email:       e.user.email,
      user_birthDay:    "",
      user_birthMonth:  "",
      user_birthYear:   "",
      user_gender:      ""
    };
    return gigyaData;
  }

  // Note: the actual signature calculation implementation should be on server side
  createSignature(UID:any, timestamp:any) {
    let encodedUID = encodeURIComponent(UID + timestamp); // encode the UID parameter before sending it to the server.
    // On server side use decodeURIComponent() function to decode an encoded UID
    return encodedUID;
  }

  /*
  * @description  use for click tracking
   */
  onButtonClickedHander(e:any) {
   console.log('onButtonClicked: ' + e);
  }

  /**
   * do some other stuff
     */
  loginNonSocialHandler(response:any) {
    // do ajax call to my cloud server
    //let context = response.context || this;
    //context.route.navigate(['dashboard']);
    // do a serverside redirect
  }

  /**
   * to finish off
   */
  onSubmit(evt:any) {
    evt.preventDefault();
    this.loginNonSocialHandler({});
    this.submitted = true;
  }

}
