"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var login_model_1 = require("./login.model");
var user_service_1 = require("../services/user/user.service");
var gigya_session_service_1 = require("../services/gigya-session.service");
var application_service_1 = require("../services/application.service.ts/application.service");
require('rxjs/add/operator/toPromise');
var LoginFormComponent = (function () {
    function LoginFormComponent(applicationService, userService, gigyaSessionService, router, route, location) {
        this.applicationService = applicationService;
        this.userService = userService;
        this.gigyaSessionService = gigyaSessionService;
        this.router = router;
        this.route = route;
        this.location = location;
        this.model = new login_model_1.LoginModel(18, 'mrkwebley@gmail.com', 'Chuc#Over1street', '');
        this.submitted = false;
    }
    LoginFormComponent.prototype.ngOnInit = function () { };
    LoginFormComponent.prototype.onSubmit = function (evt) {
        evt.preventDefault();
        console.log('submitted:', JSON.stringify(this.model, null, 7));
        this.submitted = true;
        this.userLogin();
        // if quality data then save it: finish later
        //this.isUserAuthorised();
    };
    LoginFormComponent.prototype.newUser = function () {
        this.model = new login_model_1.LoginModel(42, '', '', '');
    };
    LoginFormComponent.prototype.goToMyProfile = function (e) {
        e.context.gigyaSessionService.createSession();
    };
    LoginFormComponent.prototype.myProfile = function () {
        this.router.navigate(['/my-profile']);
    };
    /**
     * TODO AND FINISH OFF
     */
    LoginFormComponent.prototype.userLogin = function () {
        var _this = this;
        var loginAuhtorised = this.applicationService.userLoginSession().then(function (response) {
            // return response.json();
            // 0. do load component
            // 0. set sessionStorage by sessonReturned
            // 1. get user data from db, and set in sessionStorage (encrypted)
            if (response.session.length > 10) {
                _this.router.navigate(['/my-profile']);
            }
        })
            .catch(this.handleError);
    };
    LoginFormComponent.prototype.handleError = function (error) {
        console.error('OBSERVER VERSION:- An error occurred', error);
        return Promise.reject(error.message || error);
    };
    /**
     * forget this, just finish the social login stuff
     *
     * TODO: SOLUTION,
     * SEND TO BACKEND, CREATE A SESSION, STORE THE SESSION,  THEN REDIECT THE PAGE....
     */
    LoginFormComponent.prototype.isUserAuthorised = function () {
        var that = this;
        this.userService.isUserAuthorised(this.model, this) //, this.goToMyProfile
            .then(function (result) {
            console.log('result from the serice:- ', result);
            if (result.status == 200) {
                console.info('YOU ARE authenticated, go to login');
            }
            else {
                console.warn('show error to the user, they are not authenticated');
            }
        }, function (e) {
            console.log('error after result IN isUserAuthorised FUNC');
        });
    };
    LoginFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'login-form',
            templateUrl: './login-form.component.html',
            styleUrls: ['./login.css']
        }), 
        __metadata('design:paramtypes', [application_service_1.ApplicationService, user_service_1.UserService, gigya_session_service_1.GigyaSessionService, router_1.Router, router_1.ActivatedRoute, common_1.Location])
    ], LoginFormComponent);
    return LoginFormComponent;
}());
exports.LoginFormComponent = LoginFormComponent;
//# sourceMappingURL=login-form.component.js.map