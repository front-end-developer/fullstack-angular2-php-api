import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule, Router, Route, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import {LoginModel} from "./login.model";
import {UserService} from "../services/user/user.service";
import {GigyaSessionService} from "../services/gigya-session.service";
import {ApplicationService} from "../services/application.service.ts/application.service";
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login.css']
})

export class LoginFormComponent implements OnInit {

  selectedChildUser:LoginModel;

  model = new LoginModel(18, 'mrkwebley@gmail.com', 'Chuc#Over1street', '');

  submitted = false;

  constructor(private applicationService:ApplicationService,
              private userService: UserService,
              private gigyaSessionService: GigyaSessionService,
              private router: Router,
              private route: ActivatedRoute,
              private location: Location) {

  }

  ngOnInit(): void { }

  onSubmit(evt:any) {
    evt.preventDefault();
    console.log('submitted:', JSON.stringify(this.model, null, 7));
    this.submitted = true;
    this.userLogin();

    // if quality data then save it: finish later
    //this.isUserAuthorised();
  }

  newUser() {
    this.model = new LoginModel(42, '', '', '');
  }

  goToMyProfile(e:any){
    e.context.gigyaSessionService.createSession();
  }


  myProfile(){
    this.router.navigate(['/my-profile']);
  }

  /**
   * TODO AND FINISH OFF
   */
  userLogin() {
     let loginAuhtorised = this.applicationService.userLoginSession().then((response:any) => {
         // return response.json();

         // 0. do load component
         // 0. set sessionStorage by sessonReturned
         // 1. get user data from db, and set in sessionStorage (encrypted)

         if (response.session.length > 10) {
           this.router.navigate(['/my-profile']);
         }
       })
       .catch(this.handleError);

  }

  private handleError(error: any): Promise<any> {
    console.error('OBSERVER VERSION:- An error occurred', error);
    return Promise.reject(error.message || error);
  }

  /**
   * forget this, just finish the social login stuff
   *
   * TODO: SOLUTION,
   * SEND TO BACKEND, CREATE A SESSION, STORE THE SESSION,  THEN REDIECT THE PAGE....
   */
  isUserAuthorised(){
    let that = this;
    this.userService.isUserAuthorised(this.model, this) //, this.goToMyProfile
      .then(function(result) {
          console.log('result from the serice:- ', result);
          if (result.status == 200) {
            console.info('YOU ARE authenticated, go to login');
          } else {
            console.warn('show error to the user, they are not authenticated');
          }
        },
        function(e){
          console.log('error after result IN isUserAuthorised FUNC');
        })
  }
}
