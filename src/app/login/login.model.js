"use strict";
var LoginModel = (function () {
    function LoginModel(id, nameOrEmail, password, status) {
        this.id = id;
        this.nameOrEmail = nameOrEmail;
        this.password = password;
        this.status = status;
    }
    return LoginModel;
}());
exports.LoginModel = LoginModel;
//# sourceMappingURL=login.model.js.map