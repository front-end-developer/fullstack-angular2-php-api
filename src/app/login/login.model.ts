export class LoginModel {
  constructor(
    public id: number,
    public nameOrEmail: string,
    public password: string,
    public status: string
  ) {  }
}
