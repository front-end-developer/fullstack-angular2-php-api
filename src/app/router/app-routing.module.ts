import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginFormComponent} from "./../login/login-form.component";
import {UserProfileComponent} from "./../user-profile/user-profile.component";

const routes: Routes = [
  {
    path:       '',
    redirectTo: '/login',
    pathMatch:  'full'
  },
  {
    path:       'login',
    component:  LoginFormComponent
  },
  {
    path:       'my-profile',
    component:  UserProfileComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
