import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  template:  `
  <h1>{{title}}</h1>
  <nav>
  <!--
  <a routerLink="/my-profile" routerLinkActive="active">My Profile</a>
    <a routerLink="/login" routerLinkActive="active">Login</a>
  </nav>
  -->
  <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.css'],
 })

export class AppComponent {
  title = 'Test and start of something'
}
