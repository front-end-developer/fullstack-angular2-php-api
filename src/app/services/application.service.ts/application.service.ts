import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class ApplicationService {

  private url:string = 'http://localhost:80/markwebley/api/login/';
  private headers = new Headers();

  constructor(private http: Http){
  }

  userLoginSession() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let form = {"email":"mrkwebley@gmail.com","userpwd":"codeTest#"};

    return this.http.post(this.url, JSON.stringify(form), options)
      .map( (res) => {
        return res.json();
      })
      .toPromise()
  }

  private handleError(error: any): Promise<any> {
    console.error('PROMISES VERSION:- An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
