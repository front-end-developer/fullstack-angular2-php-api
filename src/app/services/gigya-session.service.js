"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/map');
var GigyaSessionService = (function () {
    function GigyaSessionService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.requestOptions = new http_1.RequestOptions({ headers: this.headers });
        this.urlGiGyaLoginUrl = 'http://localhost:80/markwebley/api/login/gigya';
    }
    GigyaSessionService.prototype.checkSession = function () {
        return this.initAppSession();
    };
    /**
     * TODO:
     * ideally - should come from the backend using :
     *
     VERIFY USER SESSION ONLY: CHECK a user session MATCHES a current session in the db
  
     payload request:
     ajax url: http://localhost/markwebley/api/session/?session=session_keys_string_aservovi089vasrefi
  
     API RESPONSE IF SESSION EXIST:
     {
         "session_status": true,
         "status": 200
     }
  
     API RESPONSE IF SESSION DOES NOT EXIST:
     {
     "session_status": false,
     "status": 200
     }
     */
    GigyaSessionService.prototype.verifySessionKey = function () {
    };
    /**
     * POST
     * gigya data and UID to my database, DB should return a session
     */
    GigyaSessionService.prototype.dataTransform = function (e) {
        return {
            "eventName": e.eventName,
            "timestamp": e.timestamp,
            "UID": e.UID,
            "UIDSig": e.UIDSig,
            "UIDSignature": e.UIDSignature,
            "contacts": "",
            "displayname": e.user.nickname,
            "friends": "",
            "user_photoURL": e.user.photoURL,
            "user_firstname": e.user.firstName,
            "user_lastname": e.user.lastName,
            "user_email": e.user.email,
            "user_birthDay": e.user.birthDay,
            "user_birthMonth": e.user.birthMonth,
            "user_birthYear": e.user.birthYear,
            "user_gender": e.user.gender
        };
    };
    /**
     * @description   Checks if Gigya user is in the db, if not it adds it and returns a session key.
     * @returns {Promise<R>|Promise<*|T>|Promise<*>|any}
       */
    GigyaSessionService.prototype.postUserGigyaData = function (e) {
        var flatData = this.dataTransform(e);
        console.log('post To GIgya with UID: ', JSON.stringify(flatData, null, 7));
        return this.http
            .post(this.urlGiGyaLoginUrl, JSON.stringify(flatData), this.requestOptions)
            .map(function (res) {
            console.log(res);
            return res.json();
        })
            .toPromise()
            .catch(this.handleError);
    };
    GigyaSessionService.prototype.handleError = function (error) {
        console.error('PROMISES VERSION:- An error occurred', error);
        return Promise.reject(error.message || error);
    };
    /**
     * Session key that is returned from my own API, my Database
     */
    GigyaSessionService.prototype.createSession = function (session) {
        //sessionStorage.setItem('appijuiceSession', 'needstobeencryptedsessionkey');
        sessionStorage.setItem('appijuiceSession', session);
    };
    GigyaSessionService.prototype.getSessionKey = function () {
        sessionStorage.getItem('appijuiceSession');
    };
    /**
     * stores results returned from gigya and my DB
     */
    GigyaSessionService.prototype.set = function (sessionData) {
        sessionStorage.setItem('gigyaSession', sessionData);
    };
    GigyaSessionService.prototype.get = function () {
        return JSON.parse(sessionStorage.getItem('gigyaSession'));
    };
    GigyaSessionService.prototype.getUsername = function () {
        if (sessionStorage.getItem('gigyaSession')) {
            return JSON.parse(sessionStorage.getItem('gigyaSession')).username;
        }
        else {
            return '';
        }
    };
    GigyaSessionService.prototype.getFullname = function () {
        if (sessionStorage.getItem('gigyaSession')) {
            return JSON.parse(sessionStorage.getItem('gigyaSession')).firstName + ' ' + JSON.parse(sessionStorage.getItem('gigyaSession')).lastName;
        }
        else {
            return '';
        }
    };
    GigyaSessionService.prototype.getEmail = function () {
        if (sessionStorage.getItem('gigyaSession')) {
            return JSON.parse(sessionStorage.getItem('gigyaSession')).email;
        }
        else {
            return '';
        }
    };
    GigyaSessionService.prototype.getUID = function () {
        if (sessionStorage.getItem('gigyaSession')) {
            return JSON.parse(sessionStorage.getItem('gigyaSession')).UID;
        }
        else {
            return '';
        }
    };
    /**
     * clear site and gygya session
     */
    GigyaSessionService.prototype.destroy = function (gigya, callback) {
        if (gigya === void 0) { gigya = null; }
        if (callback === void 0) { callback = null; }
        sessionStorage.removeItem('gigyaSession');
        sessionStorage.removeItem('appijuiceSession');
        if (gigya != null && callback != null) {
            // logout of gigya
            gigya.accounts.logout({ callback: callback });
            // logout from the gigya platform
            gigya.socialize.logout({ callback: callback });
        }
    };
    GigyaSessionService.prototype.initAppSession = function () {
        var isSessionValid = false;
        var isGigyaUserUID = null;
        var isGigyaUserUIDSig = null;
        var isSiteSessionUser = sessionStorage.getItem('appijuiceSession');
        if (sessionStorage.getItem('gigyaSession')) {
            isGigyaUserUID = JSON.parse(sessionStorage.getItem('gigyaSession')).UID;
            isGigyaUserUIDSig = JSON.parse(sessionStorage.getItem('gigyaSession')).UIDSignature;
        }
        if (isGigyaUserUID || isSiteSessionUser) {
            console.log('user is logged in');
            isSessionValid = true;
        }
        else {
            console.log('user not logged in');
        }
        return isSessionValid;
    };
    GigyaSessionService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], GigyaSessionService);
    return GigyaSessionService;
}());
exports.GigyaSessionService = GigyaSessionService;
//# sourceMappingURL=gigya-session.service.js.map