import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class GigyaSessionService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private requestOptions = new RequestOptions({ headers: this.headers });
  private urlGiGyaLoginUrl = 'http://localhost:80/markwebley/api/login/gigya';

  constructor(private http: Http) {}

  public checkSession(){
    return this.initAppSession();
  }


  /**
   * TODO:
   * ideally - should come from the backend using :
   *
   VERIFY USER SESSION ONLY: CHECK a user session MATCHES a current session in the db

   payload request:
   ajax url: http://localhost/markwebley/api/session/?session=session_keys_string_aservovi089vasrefi

   API RESPONSE IF SESSION EXIST:
   {
       "session_status": true,
       "status": 200
   }

   API RESPONSE IF SESSION DOES NOT EXIST:
   {
   "session_status": false,
   "status": 200
   }
   */
  public verifySessionKey(){

  }

  /**
   * POST
   * gigya data and UID to my database, DB should return a session
   */

  dataTransform(e:any) {
    return {
      "eventName":        e.eventName,
      "timestamp":        e.timestamp,
      "UID":              e.UID,
      "UIDSig":           e.UIDSig,
      "UIDSignature":     e.UIDSignature,
      "contacts":         "",
      "displayname":      e.user.nickname,
      "friends":          "",
      "user_photoURL":    e.user.photoURL,
      "user_firstname":   e.user.firstName,
      "user_lastname":    e.user.lastName,
      "user_email":       e.user.email,
      "user_birthDay":    e.user.birthDay,
      "user_birthMonth":  e.user.birthMonth,
      "user_birthYear":   e.user.birthYear,
      "user_gender":      e.user.gender
    };
  }

  /**
   * @description   Checks if Gigya user is in the db, if not it adds it and returns a session key.
   * @returns {Promise<R>|Promise<*|T>|Promise<*>|any}
     */
  postUserGigyaData(e:any) {
      let flatData = this.dataTransform(e);
      console.log('post To GIgya with UID: ', JSON.stringify(flatData, null, 7) );
      return this.http
         .post(this.urlGiGyaLoginUrl, JSON.stringify(flatData), this.requestOptions)
         .map( (res ) => {
           console.log(res);
           return res.json();
         })
         .toPromise()
        .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('PROMISES VERSION:- An error occurred', error);
    return Promise.reject(error.message || error);
  }


  /**
   * Session key that is returned from my own API, my Database
   */
  public createSession(session:string) {
    //sessionStorage.setItem('appijuiceSession', 'needstobeencryptedsessionkey');
    sessionStorage.setItem('appijuiceSession', session);
  }

  public getSessionKey() {
    sessionStorage.getItem('appijuiceSession');
  }

  /**
   * stores results returned from gigya and my DB
   */
  public set(sessionData:any){
    sessionStorage.setItem('gigyaSession', sessionData );
  }

  public get() {

    return JSON.parse(sessionStorage.getItem('gigyaSession'));
  }

  public getUsername(){
    if (sessionStorage.getItem('gigyaSession')) {
      return JSON.parse(sessionStorage.getItem('gigyaSession')).username;
    } else {
      return '';
    }
  }

  public getFullname(){
    if (sessionStorage.getItem('gigyaSession')) {
      return JSON.parse(sessionStorage.getItem('gigyaSession')).firstName + ' ' + JSON.parse(sessionStorage.getItem('gigyaSession')).lastName;
    } else {
      return '';
    }
  }

  public getEmail(){
    if (sessionStorage.getItem('gigyaSession')) {
      return JSON.parse(sessionStorage.getItem('gigyaSession')).email;
    } else {
      return '';
    }
  }

  public getUID():String {
      if (sessionStorage.getItem('gigyaSession')) {
        return JSON.parse(sessionStorage.getItem('gigyaSession')).UID;
      } else {
        return '';
      }
  }

  /**
   * clear site and gygya session
   */
  public destroy(gigya:any = null, callback:any = null) {
    sessionStorage.removeItem('gigyaSession');
    sessionStorage.removeItem('appijuiceSession');

    if (gigya != null && callback != null) {
      // logout of gigya
      gigya.accounts.logout({callback:callback});

      // logout from the gigya platform
      gigya.socialize.logout({callback:callback});
    }
  }

  public initAppSession():boolean {
    let isSessionValid = false;
    let isGigyaUserUID:string = null;
    let isGigyaUserUIDSig:string = null;
    let isSiteSessionUser = sessionStorage.getItem('appijuiceSession');
    if (sessionStorage.getItem('gigyaSession')){
      isGigyaUserUID = JSON.parse(sessionStorage.getItem('gigyaSession')).UID;
      isGigyaUserUIDSig = JSON.parse(sessionStorage.getItem('gigyaSession')).UIDSignature;
    }
    if ( isGigyaUserUID || isSiteSessionUser) {
      console.log('user is logged in');
      isSessionValid = true;
    } else {
      console.log('user not logged in');
    }
    return isSessionValid;
  }
}
