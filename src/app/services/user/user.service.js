"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.requestOptions = new http_1.RequestOptions({ headers: this.headers });
        this.usersUrl = 'api/users';
        this.usersAuthUrl = 'api/user';
        this.urlGiGyaLoginUrl = 'http://localhost:80/markwebley/api/login/gigya';
        this.formTest = { "email": "mrkwebley@gmail.com", "userpwd": "codeTest#" };
    }
    UserService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    UserService.prototype.isEmailValid = function (e) {
        var regEmail = /^[A-Za-z0-9\.]{1,64}@{1}(?:[A-Za-z0-9]{1,64}\.){1,64}[A-za-z]{2,7}$/i;
        return regEmail.test(e);
    };
    /**
     * Words 6 to 8 charactors long, numbers or symbols
     **/
    UserService.prototype.isUsernameValid = function (e) {
        var regStr = /^[\w+]{5,10}$/gi;
        return regStr.test(e);
    };
    /**
     * At lowercase with at least one capital letter & one number
     * and at least one of these symbols: @#$
     * 6 to 8 charactors long
     **/
    UserService.prototype.isPasswordValid = function (e) {
        var rege = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\@\#\$]).{6,17}$/gm;
        return rege.test(e);
    };
    UserService.prototype.validateModel = function (userModel) {
        var isValid = false;
        // todo: REGEXP for email FOR userModel.nameOrEmail
        if ((this.isEmailValid(userModel.nameOrEmail) || this.isUsernameValid(userModel.nameOrEmail)) && this.isPasswordValid(userModel.password)) {
            userModel.status = '';
            isValid = true;
        }
        return isValid;
    };
    UserService.prototype.isGigyaUserAuthorised = function (gigyaUserData) {
        return this.http.post(this.urlGiGyaLoginUrl, JSON.stringify(gigyaUserData), this.requestOptions)
            .toPromise()
            .then(function (response) {
            var obj = {
                ok: response.ok,
                status: response.status,
                statusText: response.statusText
            };
            return obj;
        })
            .catch(this.handleError);
    };
    /**
     * was testing stuff but I left it in for to finish later
     * @param userModel
     * @param context
     * @returns {Promise<T>}
       */
    UserService.prototype.isUserAuthorised = function (userModel, context) {
        if (!this.validateModel(userModel)) {
            userModel.status = 'Error';
            return new Promise(function (resolve, reject) {
                resolve(userModel);
            });
        }
        var requestObject = {
            "email": userModel.nameOrEmail,
            "password": userModel.password
        };
        var url = 'http://localhost:80/markwebley/api/login';
        this.http.post(url, JSON.stringify(requestObject))
            .toPromise()
            .then(function (response) {
            var obj = {
                ok: response.ok,
                status: response.status,
                statusText: response.statusText,
                context: context
            };
            return obj;
        })
            .catch(this.handleError);
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map