import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import {LoginModel} from "../../login/login.model";

@Injectable()
export class UserService {
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private requestOptions = new RequestOptions({ headers: this.headers });
  private usersUrl = 'api/users';
  private usersAuthUrl = 'api/user';
  private urlGiGyaLoginUrl = 'http://localhost:80/markwebley/api/login/gigya';
  private formTest = {"email":"mrkwebley@gmail.com","userpwd":"codeTest#"};

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }


  isEmailValid(e:string) {
    var regEmail = /^[A-Za-z0-9\.]{1,64}@{1}(?:[A-Za-z0-9]{1,64}\.){1,64}[A-za-z]{2,7}$/i;
    return regEmail.test(e);
  }

  /**
   * Words 6 to 8 charactors long, numbers or symbols
   **/
  isUsernameValid(e:string) {
    let regStr = /^[\w+]{5,10}$/gi;
    return regStr.test(e);
  }

  /**
   * At lowercase with at least one capital letter & one number
   * and at least one of these symbols: @#$
   * 6 to 8 charactors long
   **/
  isPasswordValid(e:string) {
    let rege = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\@\#\$]).{6,17}$/gm;
    return rege.test(e);
  }

  validateModel(userModel: LoginModel): Boolean {
    let isValid = false;
    // todo: REGEXP for email FOR userModel.nameOrEmail
    if ( ( this.isEmailValid(userModel.nameOrEmail) ||  this.isUsernameValid(userModel.nameOrEmail) ) && this.isPasswordValid(userModel.password) ) {
      userModel.status = '';
      isValid = true;
    }
    return isValid;
  }

  isGigyaUserAuthorised(gigyaUserData:any): Promise<any> {
    return this.http.post(this.urlGiGyaLoginUrl, JSON.stringify(gigyaUserData), this.requestOptions)
      .toPromise()
      .then((response) => {
            var obj = {
            ok:         response.ok,
            status:     response.status,
            statusText: response.statusText
          }
          return obj;
        }
      )
      .catch(this.handleError);

  }

  /**
   * was testing stuff but I left it in for to finish later
   * @param userModel
   * @param context
   * @returns {Promise<T>}
     */
  isUserAuthorised(userModel: LoginModel, context:any): Promise<any> { // , callback:any
    if (!this.validateModel(userModel)) {
      userModel.status = 'Error';
      return new Promise((resolve, reject) => {
        resolve(userModel);
      });
    }
    let requestObject = {
      "email":      userModel.nameOrEmail,
      "password":   userModel.password
    }

    const url = 'http://localhost:80/markwebley/api/login';
    this.http.post(url, JSON.stringify(requestObject))
      .toPromise()
      .then((response) => {
          var obj = {
            ok:         response.ok,
            status:     response.status,
            statusText: response.statusText,
            context:    context
          }
          return obj;
        }
      )
      .catch(this.handleError);
  }
}
